#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright 2017 youcefsourani <youssef.m.sourani@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from os.path import basename,join
import time
import subprocess
import dbus
import string
import os




    
def __l():
    os.system("clear")
    print ("************************************")
    print ("* youssef.m.sourani@gmail.com      *")
    print ("* https://arfedora.blogspot.com    *")
    print ("* Use chntpw To Edit SAM passwords *")
    print ("************************************\n")

class Patition(object):
    def __init__(self,bus,block_device):
        self.__bus           = bus
        self.block_device    = block_device
        self.__proxy         = self.__bus.get_object("org.freedesktop.UDisks2",self.block_device)
        self.__interface     = dbus.Interface(self.__proxy,"org.freedesktop.DBus.Properties")
        self.NAME            = "/dev/"+basename(self.block_device)
        self.SIZE            = self.__interface.Get("org.freedesktop.UDisks2.Block","Size")
        self.TYPE            = self.__interface.Get("org.freedesktop.UDisks2.Block","IdType")
        self.SELF            = self
        
        self.__umount_interface = dbus.Interface(self.__proxy,"org.freedesktop.UDisks2.Filesystem")
        self.__umount = self.__umount_interface.get_dbus_method("Unmount")
        
        self.__mount_interface = dbus.Interface(self.__proxy,"org.freedesktop.UDisks2.Filesystem")
        self.__mount = self.__mount_interface.get_dbus_method("Mount")
            
    def umount_(self,force):
        try:
            return self.__umount({"force" : force})
        except Exception as e :
            return e._dbus_error_name


    def mount_(self,fstype=None,no_user_interaction=False):
        try:
            if fstype:
                return self.__mount({"fstype" : fstype,"auth.no_user_interaction":no_user_interaction})
            else:
                return self.__mount({"fstype" : self.TYPE,"auth.no_user_interaction":no_user_interaction})
        except dbus.exceptions.DBusException  as e :
            return e._dbus_error_name
        

class Drive(object):
    def __init__(self,bus,block_device):
        self.__bus           = bus
        self.__block_device  = block_device
        self.__proxy         = self.__bus.get_object("org.freedesktop.UDisks2",self.__block_device)
        self.__interface     = dbus.Interface(self.__proxy,"org.freedesktop.DBus.Properties")
        self.ALLPATTIONS     = self.__all_parttions()
        
    def __all_parttions(self):
        result = []
        p = self.__interface.Get("org.freedesktop.UDisks2.PartitionTable","Partitions")
        for i in p:
            try:
                result.append(Patition(self.__bus,i))
            except:
                pass
        return result

        

def INIT():
    bus = dbus.SystemBus()
    result  = []
    for char in string.ascii_lowercase :
        try:
            bus.get_object("org.freedesktop.UDisks2","/org/freedesktop/UDisks2/block_devices/sd{}".format(char))
            result.append(Drive(bus,"/org/freedesktop/UDisks2/block_devices/sd{}".format(char)))
        except :
            pass
    return result
    
    
    
def get_parttions_by_type(type_):
    bus = dbus.SystemBus()
    result = dict()
    for d in INIT():
        for i in d.ALLPATTIONS:
            if "all" in type_:
                result.setdefault(i.NAME,[i.SELF,i.SIZE,i.TYPE])
            elif i.TYPE in type_:
                result.setdefault(i.NAME,[i.SELF,i.SIZE,i.TYPE])
    return result



def check_windows_folder():
    result = {}
    count  = 1
    all_ntfs_parrtions = get_parttions_by_type("ntfs")
    for k,v in all_ntfs_parrtions.items():
        v[0].umount_(force=True)
        subprocess.call("ntfsfix {} &>/dev/null".format(k),shell=True)
        mount_point = v[0].mount_() 
        if mount_point[0]!="/":
            v[0].umount_(force=True)
            mount_point = v[0].mount_()
        if os.path.isdir(os.path.join(mount_point,"Windows/System32/config")) :
            result.setdefault(str(count),[k,os.path.join(mount_point,"Windows/System32/config")])
            count+=1
        elif os.path.isdir(os.path.join(mount_point,"windows/System32/config")):
            result.setdefault(str(count),[k,os.path.join(mount_point,"windows/System32/config")])
            count+=1
    return result

def get_all_user_name(location):
    result = {}
    count  = 1
    black_list_user = ["DefaultAccount","Guest","WDAGUtilityAccount"]
    os.chdir(location)
    out = subprocess.check_output("chntpw -l  SAM",shell=True).decode("utf-8").split("\n")
    for line in out :
        line = line.strip()
        if line.startswith("|") and not line.startswith("| RID -|"):
            line = line.split("|")
            user = line[2].strip()
            if user not in black_list_user:
                result.setdefault(str(count),user)
                count+=1
    return result


if __name__ == "__main__":
    if not  os.path.isfile("/usr/bin/chntpw"):
        print("chntpw Not Found.")
        exit(12)
        
    if os.getuid()!=0:
        print("Run Script With Root Permissions.")
        exit(11)
        
    all_windows_parrtions = check_windows_folder()
    
    if len(all_windows_parrtions)==0:
        print("Windows Not Found.\n")
        exit(3)
        
    else:
        while True:
            __l()
            for k,v in all_windows_parrtions.items():
                print("{}- {}".format(k,v[0]))
            print("\nChoise Windows Parttion || Q To Quit : ")
            answer = input("- ")
            if answer == "q" or answer=="Q":
                print("Bye..\n")
                exit(10)
            
            elif answer in all_windows_parrtions.keys():
                all_user = get_all_user_name(all_windows_parrtions[answer][-1])
                while True:
                    __l()
                    for id_,username in all_user.items():
                        print("{}- {}".format(id_,username))
                    print("\nChoise User Q To Quit : ")
                    answer = input("- ")
                    if answer == "q" or answer=="Q":
                        print("Bye..\n")
                        exit(10)
                    elif answer in all_user.keys():
                        subprocess.call("chntpw -u {}  SAM".format(all_user[answer]),shell=True)
                        exit()

                


